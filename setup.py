from setuptools import setup, find_packages

REQUIRED_PACKAGES = [
    'tensorflow==1.11.0',
    'tensorflow-serving-api==1.11.0',
    'pandas==0.23.4',
]


setup(name='rcomtfutils',
      version='1.0.0',
      packages=find_packages(exclude=('tests',)),
      url='https://bitbucket.org/truedmp/rcomtfutils',
      include_package_data=True,
      description='Rcom tensorflow utils',
      author='Patcharin Cheng',
      author_email='patcharin.che@truedigital.com',
      install_requires=REQUIRED_PACKAGES,
      zip_safe=False)

