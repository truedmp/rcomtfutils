import os
import time
import numpy as np
import unittest as ut
from collections import namedtuple
from rcomtfutils import common_utils as cu

REAL_COL_NAME = "real_world_id"
EMB_COL_NAME = "model_id"
cwd = os.path.dirname(os.path.realpath(__file__))
MAPPING_FILE = os.path.join(cwd, "dnn_test_mapping.csv")
Mapper = namedtuple('Mapper', ['real_world_id', 'model_id'])
MAPPINGS = [Mapper('vdo_harry_potter', 1), Mapper('vdo_test', 2)]


class CommonUtilsTest(ut.TestCase):

    def test_load_from_csv_file(self):
        df = cu.load_from_csv_file(MAPPING_FILE)
        self.assertEqual(len(df), 10)

    def test_pad_sequences_pre_padding(self):
        watch_hist = [[1, 2, 3]]
        exp = [[0, 0, 1, 2, 3]]
        actual = cu.pad_sequences(watch_hist, 5, padding='pre', truncating='pre', value=0)
        self.assertTrue(np.array_equal(actual, exp))

    def test_pad_sequences_post_padding(self):
        watch_hist = [[1, 2, 3]]
        exp = [[1, 2, 3, 0, 0]]
        actual = cu.pad_sequences(watch_hist, 5, padding='post', truncating='pre', value=0)
        self.assertTrue(np.array_equal(actual, exp))

    def test_pad_sequences_pre_truncate(self):
        watch_hist = [[1, 2, 3, 4, 5]]
        exp = [[3, 4, 5]]
        actual = cu.pad_sequences(watch_hist, 3, padding='pre', truncating='pre', value=0)
        self.assertTrue(np.array_equal(actual, exp))

    def test_sort_prediction(self):
        pred = np.array([1, 2, 3, 4, 5])
        ids_actual, vals_actual = cu.sort_prediction(pred)
        self.assertTrue(np.array_equal(ids_actual, np.array([4, 3, 2, 1, 0])))
        self.assertTrue(np.array_equal(vals_actual, np.array([5, 4, 3, 2, 1])))

    def test_sort_prediction_topK(self):
        pred = np.array([7, 1, 4, 9, 18])
        ids_actual, vals_actual = cu.sort_prediction(pred, topK=3)
        self.assertTrue(np.array_equal(ids_actual, np.array([4, 3, 0])))
        self.assertTrue(np.array_equal(vals_actual, np.array([18, 9, 7])))

    def test_IdMapper_map_model_ID_to_real_world_ID(self):
        mapper = cu.IdMapper(MAPPINGS, REAL_COL_NAME, EMB_COL_NAME)
        model_ids = [1, 2]
        real_ids = mapper.to_real_world_id(model_ids)
        self.assertEqual(len(real_ids), 2)
        self.assertEqual(real_ids, ['vdo_harry_potter', 'vdo_test'])

    def test_IdMapper_map_real_world_ID_to_model_ID(self):
        mapper = cu.IdMapper(MAPPINGS, REAL_COL_NAME, EMB_COL_NAME)
        real_ids = np.array(['vdo_harry_potter', 'vdo_test'])
        model_ids = mapper.to_model_id(real_ids)
        self.assertTrue(model_ids, [1, 2])

    def test_get_tod_dow_at_timezone_utc(self):
        # get current time at local timezone
        utc_hour_offset = time.timezone / (60 * 60)
        (_, _, _, local_hour, _, _, local_wday, _, _) = time.localtime()
        utc_hour, utc_dow = cu.get_tod_dow_at_timezone()
        self.assertEqual(utc_hour, local_hour + utc_hour_offset)
        self.assertTrue(utc_dow.isupper())
        

    def test_get_tod_dow_at_timezone_utc7(self):
        (_, _, _, local_hour, _, _, local_wday, _, _) = time.localtime()
        utc_hour, utc_dow = cu.get_tod_dow_at_timezone()
        self.assertEqual(utc_hour, local_hour)
        self.assertTrue(utc_dow.isupper())


if __name__ == '__main__':
    ut.main()