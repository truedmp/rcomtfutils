#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Dec  4 11:28:59 2016

@author: Champ, modified by Robroo / Coco
"""
import grpc
import time
import numpy as np
import pandas as pd
import tensorflow as tf

from rcomloggingutils.applog_utils import AppLogger
from datetime import datetime
from pytz import timezone
from tensorflow_serving.apis import predict_pb2, prediction_service_pb2_grpc, get_model_status_pb2, model_service_pb2_grpc
from tensorflow.python.keras.preprocessing.sequence import pad_sequences as pad_sequences_


_logger = AppLogger(name=__name__)


def get_file_stream(path, mode="r"):
    """Get a file stream object pointing to a file stored on Google Cloud Storage (if the path begins with "gs://"),
    or to local file.

    Parameters
    ----------
    path : str
        path to file stored on GCS (begins with "gs://") or to local file.

    mode : file mode, default to 'r'.

    Returns
    -------
    file stream object to a file on GCS, or to local file.
    """
    if path.startswith("gs://"):
        return tf.gfile.Open(path, mode=mode)
    else:
        return open(path, mode=mode)


def glob(filename):
    """Returns a list of files that match the given pattern(s).

    Parameters
    ----------
    filename: str or iterable of strings.
        the glob pattern(s).
    """
    return tf.gfile.Glob(filename)


def load_from_csv_file(file, **kwargs):
    """Loads a CSV file and returns as a dataframe

    Parameters
    ----------
    file : str
        path to CSV file stored on GCS (begins with "gs://") or locally.
    """
    _logger.info("Loading CSV file at %s", file)

    with get_file_stream(file, mode='rb') as fs:
        return pd.read_csv(fs, **kwargs)


def pad_sequences(sequences, maxlen, padding='pre', truncating='pre', value=0, dtype='int32'):
    """Pads sequences to the same length.

    see https://keras.io/preprocessing/sequence/#pad_sequences

    Parameters
    ----------
    sequences : array
        List of lists where each element is a sequence.

    maxlen : int
        maximum length of all sequences.

    padding : str, 'pre' or 'post'
        pad either before or after each sequence.

    truncating : str, 'pre' or 'post'
        remove values from sequences larger than maxlen, either at the beginning or at the end of the sequences.

    value: int
        padding value.
    """
    return pad_sequences_(sequences, maxlen=maxlen, padding=padding, truncating=truncating, value=value, dtype=dtype)


def sort_prediction(pred, topK=None):
    """Returns indices and their corresponding values sorted by values in descending order.

    Parameters
    ----------
    pred : array-like
        an 1-d array.

    topK : int, default None
        considers only the top K elements. None means considers all elements.
    """
    if topK:
        pred_ids = np.argsort(pred)[::-1][:topK]
    else:
        pred_ids = np.argsort(pred)[::-1]
    pred_scores = pred[pred_ids]
    return pred_ids, pred_scores


def get_tod_dow_at_timezone(tz_str="UTC"):
    """
    Get the current time's hour of day (0-23) and day of week (MONDAY, TUESDAY, ...) in the specified timezone

    Parameters
    ----------
    tz_str  The timezone string, defaulted to "UTC". If you want to set it to Thai, use "Asia/Bangkok"

    Returns
    -------
    hour    hour number (0-23)
    dow     str representation of the day of week
    """
    utc_tz = timezone(tz_str)
    utc_time = datetime.now(utc_tz)
    return utc_time.hour, utc_time.strftime("%A").upper()


def get_current_time_ms():
    """returns current time in millisecond"""
    return int(time.time() * 1000)


class TfModel:
    """Tensorflow Serving client. It provides a straightforward gRPC communication between Python and Tensorflow
    Serving system. See example below.

        inputs = {
            'watched': get_watch_history(ssoId)
            'day_period': get_day_period(),
            "day_of_week": get_day_of_week(),
            "watch_category": get_categories(ssoId),
            "watch_actor": get_actors(ssoId),
            "watch_country": get_countries(ssoId),
            "watch_director": get_directors(ssoId)
        }

        model = TfModel(mst.RCOM_MODEL_HOST, int(mst.RCOM_MODEL_PORT))
        pred_classes, pred_scores = model.predict_request(inputs, model_spec='movie', signature_name='predict')

    Parameters
    ----------
    host : str
        The name of the remote host to which to connect.

    port : int
        The port of the remote host to which to connect.
    """
    def __init__(self, host, port):
        _logger.info("Connecting to Tensorflow Serving at host %s, port %d", host, port)
        addr = str(host) + ':' + str(port)
        self.channel = grpc.insecure_channel(addr)
        self.stub = prediction_service_pb2_grpc.PredictionServiceStub(self.channel)

    def get_model_status(self, model_spec, timeout=5.0):
        """returns model status.

        Parameters
        ----------
        model_spec : str
            name of the model deployed in Tensorflow model server.

        timeout : float, default 5.0
            request timeout.

        Returns
        -------
        out: dict
            a dictionary containing information about model_spec, status, and version of the model.
        """
        request = get_model_status_pb2.GetModelStatusRequest()
        request.model_spec.name = model_spec

        stub = model_service_pb2_grpc.ModelServiceStub(self.channel)
        response = stub.GetModelStatus(request, timeout)
        model_version = response.model_version_status[0].version
        # we are only interested in status == 30 meaning the model is AVAILABLE,
        # see https://github.com/tensorflow/serving/blob/master/tensorflow_serving/apis/get_model_status.proto
        # for more status
        model_status = 'AVAILABLE' if response.model_version_status[0].state == 30 else 'UNAVAILABLE'
        return {'model_spec': model_spec, 'status': model_status, 'version': model_version}

    def predict_request(self, inputs, model_spec, signature_name, classes_alias='classes', scores_alias='scores', timeout=5.0):
        """sends predict request to Tensorflow model server.

        Parameters
        ----------
        inputs : dict
            a dictionary containing inputs to the Tensorflow model server. Each key corresponds to the one defined
            in Tensorflow Serving model signature and the corresponding value is the feature.

        model_spec : str
            name of the model deployed in Tensorflow model server.

        signature_name : str
            a signature name defined in Tensorflow Serving. The model signature defines inputs, outputs and type of
            prediction.

        classes_alias : str, default 'classes'
            an alias to get predictions in a response.

        scores_alias : str, default 'scores'
            an alias to get prediction scores in a response.

        timeout : float, default 5.0
            request timeout.

        Returns
        -------
        (classes, scores): tuple
            a tuple of array of predictions and array of scores
        """
        request = predict_pb2.PredictRequest()
        request.model_spec.name = model_spec
        request.model_spec.signature_name = signature_name
        for alias, X in inputs.items():
            request.inputs[alias].CopyFrom(tf.contrib.util.make_tensor_proto(X))
        pred = self.stub.Predict(request, timeout)
        scores = tf.make_ndarray(pred.outputs[scores_alias])
        classes = tf.make_ndarray(pred.outputs[classes_alias])
        # tf.make_ndarray returns bytes for string, manual decoding is required.
        classes_dec = [o.decode("utf-8") for o in classes[0]]
        return classes_dec, scores[0]


class IdMapper:
    """An ID mapper class, mapping between real-world item ID and model ID (e.g. embedding or target ID).

    Parameters
    ----------
    rows : cassandra.cluster.ResultSet
        iterator over the rows.

    real_col_name : str
        name of the real-world ID column.

    mod_col_name : str
        name of the model ID column.
    """
    def __init__(self, rows, real_col_name, mod_col_name):
        self._idx2label = {getattr(row, mod_col_name): getattr(row, real_col_name) for row in rows}
        self._label2idx = {real_world_id: model_id for model_id, real_world_id in self._idx2label.items()}

    def to_real_world_id(self, ids):
        """map model ID's to real world ID's.

        Parameters
        ----------
        ids : array
            array of model ID's.

        Returns
        -------
        out : array
            array of real world ID's.
        """
        return [self._idx2label[o] for o in ids]

    def to_model_id(self, ids):
        """map real world ID's to model ID's.

        Parameters
        ----------
        ids : array
            array of real world ID's.

        Returns
        -------
        out : array
            array of model ID's.
        """
        return [self._label2idx[o] for o in ids]
